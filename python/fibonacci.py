#! /usr/bin/env python3

import sys

def fibonacci(arg):
    if arg <= 1: return arg
    return fibonacci(arg - 1) + fibonacci(arg - 2)

assert fibonacci(0) == 0
assert fibonacci(1) == 1
assert fibonacci(2) == 1
assert fibonacci(3) == 2
assert fibonacci(7) == 13
assert fibonacci(12) == 144

if len(sys.argv) <= 1:
    print("Please provide an argument")
else:
    num = int(sys.argv[1])
    print("fib({0}) = {1}".format(num, fibonacci(num)))

