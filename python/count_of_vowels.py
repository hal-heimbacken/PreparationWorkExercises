#! /usr/bin/env python3

def count_of_vowels(arg):
    vowels = "aeiouAEIOU"
    count = 0
    for c in arg:
        if c in vowels: count = count + 1
    return count

assert count_of_vowels("Propulsion Academy") == 7

