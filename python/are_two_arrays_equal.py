#! /usr/bin/env python3

arr1 = [2, 5, 7, 9, 11]
arr2 = [2, 5, 7, 8, 11]
arr3 = [2, 5, 11, 9, 7]

def are_two_arrays_equal(arg1, arg2):
    #return sorted(arg1) == sorted(arg2)
    if len(arg1) == 0 and len(arg2) == 0:
        return True
    elif len(arg1) == 0:
        return False
    else:
        if arg1[0] not in arg2: return False
        index = arg2.index(arg1[0])
        return are_two_arrays_equal(arg1[1:], arg2[:index] + arg2[index + 1:])

assert not are_two_arrays_equal(arr1, arr2)
assert are_two_arrays_equal(arr1, arr3)

