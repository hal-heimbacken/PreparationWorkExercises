#! /usr/bin/env python3

def unique_array(arg):
    #return list(set(arg))  # the order wouldn't be the one we put in the assert's...
    ret = []
    for e in arg:
        if e not in ret: ret.append(e)
    return ret

assert unique_array([0, 3, -2, 4, 3, 2]) == [0, 3, -2, 4, 2]
assert unique_array([10, 22, 10, 20, 11, 22]) == [10, 22, 20, 11]

