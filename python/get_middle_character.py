#! /usr/bin/env python3

import math

def get_middle_character(arg):
    i1 = math.floor((len(arg) - 1)/2)
    i2 = math.ceil((len(arg) - 1)/2)
    return arg[i1:i2 + 1]

assert get_middle_character("3500") == "50"
assert get_middle_character("350") == "5"

