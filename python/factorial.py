#! /usr/bin/env python3

import sys

def factorial(n):
    #if n == 0: return 1
    #return n * factorial(n - 1)
    ret = 1
    while (n >= 1):
        ret = ret * n
        n = n - 1
    return ret

assert factorial(0) == 1
assert factorial(1) == 1
assert factorial(2) == 2
assert factorial(4) == 24
assert factorial(5) == 120

if len(sys.argv) <= 1:
    print("Please provide an argument")
else:
    num = int(sys.argv[1])
    for i in range(0, num + 1):
        print("{0}! = {1}".format(i, factorial(i)))

