#! /usr/bin/env python3

from functools import reduce

def find_digit(number, count = 0):
    if number < 10: return count
    num_list = [int(e) for e in list(str(number))]
    return find_digit(reduce(lambda x, y: x * y, num_list, 1), count + 1)

# Note: the following doesn't need any import from functools:
#print(list(map(lambda x: x*x, [0, 1, 2, 3, 4, 5])), list(filter(lambda x: x%2 == 0, [0, 1, 2, 3, 4, 5])))

assert find_digit(57) == 3
assert find_digit(5923) == 2
assert find_digit(90) == 1
assert find_digit(7) == 0
assert find_digit(999) == 4

