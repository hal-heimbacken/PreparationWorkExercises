

class Player:

    def __init__(self, name):
        self.name = name
        self.tracks = []
        self.current = 0

    def add(self, track):
        self.tracks.append(track)

    def getCurrentTrack(self):
        return self.tracks[self.current]

    def selectTrack(self, number):
        idx = number - 1
        if idx >= 0 and idx < len(self.tracks): self.current = idx

    def next(self):
        if len(self.tracks) == 0: return
        self.current = (self.current + 1) % len(self.tracks)

    def previous(self):
        if len(self.tracks) == 0: return
        if self.current == 0: self.current = len(self.tracks) - 1
        else: self.current = self.current - 1

    def play(self):
        try:
            track = self.getCurrentTrack()
            track.play()
        except IndexError:
            print("No track found!")

    def playAll(self):
        currTrack = 1
        while currTrack <= len(self.tracks):
            self.selectTrack(currTrack)
            self.play()
            currTrack = currTrack + 1

    def printTracks(self):
        i = 1
        for track in self.tracks:
            print("Track {0}: {1}".format(i, track.getInfo()))
            i = i + 1


if __name__ == "__main__":

    # make sure the following operations don't crash and check the output...

    from track import Track

    driveTrack = Track('Incubus', 'Drive', 'Make Yourself')
    laBambaTrack = Track('Ritchie Valens', 'La Bamba', 'La Bamba')

    player = Player()

    player.play()

    player.add(driveTrack)
    player.add(laBambaTrack)

    print("")
    player.play()
    player.next()
    player.play()
    player.next()
    player.play()
    player.previous()
    player.play()

    print("")
    player.playAll()

    print("")
    player.printTracks()

