

class Track:

    def __init__(self, artist, title, album):
        self.artist = artist
        self.title = title
        self.album = album

    def play(self):
        print("Playing: {0} by {1}".format(self.title, self.artist))

    def getInfo(self):
        return "Artist: {0}, title: {1}, album: {2}".format(self.artist, self.title, self.album)


if __name__ == "__main__":

    driveTrack = Track('Incubus', 'Drive', 'Make Yourself')
    laBambaTrack = Track('Ritchie Valens', 'La Bamba', 'La Bamba')

    assert driveTrack.getInfo() == "Artist: Incubus, title: Drive, album: Make Yourself"
    assert laBambaTrack.getInfo() == "Artist: Ritchie Valens, title: La Bamba, album: La Bamba"

