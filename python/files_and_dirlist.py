#! /usr/bin/env python3

import sys
import os

def files_and_dirlist(dir):
    itemList = []
    for item in os.listdir(dir):
        if item != ".git":
            itemPath = os.path.join(dir, item);
            itemList.append(itemPath)
            if os.path.isdir(itemPath):
                itemList = itemList + files_and_dirlist(itemPath)
    return itemList

content = files_and_dirlist(os.curdir) if len(sys.argv) <= 1 else files_and_dirlist(sys.argv[1])

for i in content:
    print(i)

