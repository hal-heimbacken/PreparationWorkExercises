#! /usr/bin/env python3

def get_pentagonal_numbers(n):
    ret = []
    for i in range(1, n + 1):
        ret.append((3*i*i - i) // 2)   # //: integer division
    return ret

print(get_pentagonal_numbers(50))

