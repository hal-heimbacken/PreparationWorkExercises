#! /usr/bin/env python3

from math import floor

def sort_it(array):
    even_nums = [e for e in array if floor(e) % 2 == 0]
    odd_nums = [e for e in array if floor(e) % 2 != 0]
    #return sorted(odd_nums) + sorted(even_nums, reverse = True)
    return sorted(odd_nums) + sorted(even_nums, key = (lambda x: -x))

assert sort_it([1, 2, 3, 4, 5, 6, 7, 8, 9]) == [1, 3, 5, 7, 9, 8, 6, 4, 2]
assert sort_it([26.66, 24.01, 52.00, 2.10, 44.15, 1.02, 11.15]) == [1.02, 11.15, 52.0, 44.15, 26.66, 24.01, 2.1]

