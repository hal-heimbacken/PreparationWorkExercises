#! /usr/bin/env python3

import re

def is_valid_password(arg):
    if len(arg) >= 10  \
       and re.match(r'[a-zA-Z0-9]*$', arg)  \
       and re.match(r'.*[A-Z]', arg):           # the pattern must match a string prefix!!...
        return True
    return False

assert not is_valid_password("1aaaaaaaa")
assert not is_valid_password("1aaaaaaaaa")
assert     is_valid_password("1aaaaaaaaA")
assert not is_valid_password("1aaaa_aaaA")
assert not is_valid_password("1aaaaaaaA$")

