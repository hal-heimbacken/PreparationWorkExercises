#! /usr/bin/env python3

def is_prime(num):
    if num <= 1: return False
    for i in range(2, (num // 2) + 1):
        if num % i == 0: return False
    return True

def prime_numbers(n):
    nums = range(0, n + 1)
    return [e for e in nums if is_prime(e)]

assert prime_numbers(20) == [2, 3, 5, 7, 11, 13, 17, 19]
assert prime_numbers(23) == [2, 3, 5, 7, 11, 13, 17, 19, 23]

