#! /usr/bin/env python3

def find_second_smallest(arg):
    if len(arg) < 2: return None
    smallest = min(arg)
    index = arg.index(smallest)
    arg = arg[:index] + arg[index + 1:]
    return min(arg)

assert find_second_smallest([0, 3, -2, 4, 3, 2]) == 0
assert find_second_smallest([10, 22, 10, 20, 11, 22]) == 10
assert find_second_smallest([1]) == None

