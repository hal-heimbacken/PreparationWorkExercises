#! /usr/bin/env python3

import sys

def hello():
    if len(sys.argv) <= 1:          # './hello.py' is the first argument
        print("hello, Unknown!")
    else:
        for name in sys.argv[1:]:
            print("Hello, " + name + "!")

hello()

