#! /usr/bin/env python3

def binary_repr(num):
    ret = ""
    while True:
        if num == 0:
            if ret == "": ret = "0"
            break
        ret = str(num % 2) + ret
        num = num // 2
    return ret

def get_size_of_longest_sequence_of_zeros(arg):
    currLen = 0
    lengths = []
    for d in binary_repr(arg):
        if d == "0":
            currLen = currLen + 1
        else:
            lengths.append(currLen)
            currLen = 0
    lengths.append(currLen)
    return max(lengths)


assert binary_repr(0) == "0"
assert binary_repr(1) == "1"
assert binary_repr(2) == "10"

assert binary_repr(7) == "111"
assert binary_repr(8) == "1000"
assert binary_repr(457) == "111001001"
assert binary_repr(40) == "101000"
assert binary_repr(12546) == "11000100000010"

assert get_size_of_longest_sequence_of_zeros(0) == 1
assert get_size_of_longest_sequence_of_zeros(1) == 0
assert get_size_of_longest_sequence_of_zeros(2) == 1

assert get_size_of_longest_sequence_of_zeros(7) == 0
assert get_size_of_longest_sequence_of_zeros(8) == 3
assert get_size_of_longest_sequence_of_zeros(457) == 2
assert get_size_of_longest_sequence_of_zeros(40) == 3
assert get_size_of_longest_sequence_of_zeros(12546) == 6

