

'use strict';

const assert = require("assert");


class Player {

    constructor() {
        this.tracks = [];
        this.current = 0;
    }

    add(track) {
        this.tracks.push(track);
    }
    getCurrentTrack() {
        return this.tracks[this.current];
    }
    play() {
        let track = this.getCurrentTrack();
        if (track == undefined) console.log("No track found!");
        else track.play();
    }
    playNextDiffered() {
        this.to = setTimeout(() => {this.next(); this.play(); this.playNextDiffered();}, 2000);
    }
    playAll() {
        if (this.tracks.length == 0) return;
        this.selectTrack(1);
        this.play();
        this.playNextDiffered();
    }
    stop() {
        clearTimeout(this.to);
    }
    next() {
        if (this.tracks.length == 0) return;
        this.current = (this.current + 1) % this.tracks.length;
    }
    previous() {
        if (this.tracks.length == 0) return;
        if (this.current == 0) this.current = this.tracks.length - 1;
        else this.current -= 1;
    }
    selectTrack(number) {
        let idx = number - 1;
        if (idx >= 0 && idx < this.tracks.length) this.current = idx;
    }
    printTracks() {
        let i = 1;
        for (let track of this.tracks) {
            console.log(`Track ${i}:`, track.getInfo());
            i++;
        }
    }
}

let player = new Player();


class Track {

    constructor(artist, title, album) {
        this.artist = artist;
        this.title = title;
        this.album = album;
    }

    play() {
        console.log(`Playing: ${this.title} by ${this.artist}`);
    }
    getInfo() {
        return `Artist: ${this.artist}, title: ${this.title}, album: ${this.album}`;
    }
}

let driveTrack = new Track('Incubus', 'Drive', 'Make Yourself');
let laBambaTrack = new Track('Ritchie Valens', 'La Bamba', 'La Bamba');

player.add(driveTrack);
player.add(laBambaTrack);


player.printTracks();
console.log();
player.playAll();

setTimeout(() => player.stop(), 10100);

