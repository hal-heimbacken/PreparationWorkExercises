

'use strict';

const assert = require("assert");


class Bank {
    constructor() {
        this.customers = Object.create(null);
    }

    addCustomer(cust) {
        this.customers[cust] = 0;
    }
    getAccount(cust) {
        return this.customers[cust];
    }
    printAccount(cust) {
        console.log(`${cust} account balance is ${this.getAccount(cust)}`);
    }
    printAllAccounts() {
        for (let cust of Object.keys(this.customers)) this.printAccount(cust);
    }
    deposit(cust, amount) {
        this.customers[cust] += amount;
    }
    withdraw(cust, amount) {
        this.customers[cust] -= amount;
        if (this.customers[cust] < 0) {
            this.customers[cust] += amount;
            console.log(`${cust}: not enough money on account!`);
        }
    }
}


let bank = new Bank();
bank.addCustomer('Sheldon');
assert(bank.getAccount('Sheldon') == 0);
bank.deposit('Sheldon', 10);
assert(bank.getAccount('Sheldon') == 10);
bank.addCustomer('Raj');
assert(bank.getAccount('Raj') == 0);
bank.deposit('Raj', 10000);
assert(bank.getAccount('Raj') == 10000);
bank.withdraw('Raj', 100);
assert(bank.getAccount('Sheldon') == 10);
assert(bank.getAccount('Raj') == 9900);

bank.printAccount('Sheldon');
bank.printAccount('Raj');

bank.withdraw('Raj', 1000000);
assert(bank.getAccount('Raj') == 9900);

bank.printAllAccounts();

