

'use strict';

const assert = require("assert");


function reverse(string) {
    let res = "";
    for (let i = string.length - 1; i >= 0; i--)
        res += string[i];
    return res;
}
assert(reverse("Propulsion Academy") == "ymedacA noisluporP");
assert(reverse("Hello") == "olleH");
assert(reverse("abcd") == "dcba");
assert(reverse("a") == "a");
assert(reverse("") == "");


function factorial(n) {
    if (n < 0) return undefined;
    if (n == 0) return 1;
    return n * factorial(n - 1);
}
assert(factorial(5) == 120);
assert(factorial(4) == 24);
assert(factorial(0) == 1);
assert(factorial(-1) === undefined);


function longest_word(sentence) {
    let words = sentence.split(" ");
    if (words.length == 0) return undefined;
    let res = words[0];
    for (let word of words) {
        if (word.length > res.length) res = word;
    }
    return res;
}
assert(longest_word("This is an amazing test") == "amazing");
assert(longest_word("Laurent Colin") == "Laurent");
assert(longest_word("Propulsion 123") == "Propulsion");


function sum_nums(num) {
    let res = 0;
    while (num > 0) {
        res += num--;
    }
    return res;
}
assert(sum_nums(6) == 21);
assert(sum_nums(1) == 1);
assert(sum_nums(0) == 0);


function time_conversion(minutes) {
    let mins = String(minutes % 60).padStart(2, '0');
    let hours = String(Math.floor(minutes / 60)).padStart(2, '0');
    return hours + ":" + mins;
}
assert(time_conversion(155) == "02:35");
assert(time_conversion(61) == "01:01");
assert(time_conversion(60) == "01:00");
assert(time_conversion(59) == "00:59");


function count_vowels(string) {
    string = string.toLowerCase();
    let vowels = "aeiou";
    let res = 0;
    for (let char of string) {
        if (vowels.indexOf(char) > -1) res++;
    }
    return res;
}
assert(count_vowels("alphabet") == 3);
assert(count_vowels("Propulsion Academy") == 7);
assert(count_vowels("AaaAa") == 5);
assert(count_vowels("fly") == 0);


function palindrome(string) {
    //return reverse(string) == string;
    for (let i = 0; i < (string.length - 1) / 2; i++) {
        if (string[i] != string[string.length - 1 - i]) return false;
    }
    return true;
}
assert(palindrome("ABBA"));
assert(palindrome("AbbA"));
assert(!palindrome("abcd"));
assert(!palindrome("abca"));
assert(!palindrome("abada"));
assert(palindrome("aa"));
assert(palindrome("aba"));
assert(palindrome("a"));
assert(palindrome(""));


function nearby_az(string) {
    let subStrings = [];
    for (let i = 0; i < string.length; i++) {
        if (string[i] == "a") subStrings.push(string.slice(i, i + 4))
    }
    return subStrings.some(s => s.indexOf("z") > -1);
}
assert(!nearby_az("abbbz"));
assert(nearby_az("abz"));
assert(nearby_az("abcz"));
assert(!nearby_az("abba"));
assert(nearby_az("abbabbz"));


function two_sum(nums) {
    let res = [];
    for (let i = 0; i < nums.length - 1; i++) {
        for (let j = i + 1; j < nums.length; j++) {
            if (nums[i] + nums[j] == 0) res.push([i, j]);
        }
    }
    return res;
}
assert(JSON.stringify(two_sum([1, 3, -1, 5])) == "[[0,2]]");
assert(JSON.stringify(two_sum([1, 3, -1, 5, -3])) == "[[0,2],[1,4]]");
assert(JSON.stringify(two_sum([1, 5, 3, -4])) == "[]");


function is_power_of_two(num) {
    if (num == 2) return true;
    if (num == 0 || num % 2 == 1) return false;
    return is_power_of_two(num / 2);
}
assert(is_power_of_two(8));
assert(is_power_of_two(16));
assert(is_power_of_two(32));
assert(!is_power_of_two(12));
assert(!is_power_of_two(24));
assert(!is_power_of_two(1));  // !...


function repeat_string_num_times(str, num) {
    //return str.repeat(num);
    if (num < 0) return undefined;  // repeat() would break!
    let res = "";
    while (num > 0) {
        res += str;
        num--;
    }
    return res;
}
assert(repeat_string_num_times("abc", 3) == 'abcabcabc');
assert(repeat_string_num_times("abc", 1) == 'abc');
assert(repeat_string_num_times("abc", 0) == '');
assert(repeat_string_num_times("abc", -1) === undefined);


function add_all([from, to]) {
    let res = 0;
    for (let i = from; i <= to; i++) res += i;
    return res;
}
assert(add_all([1, 4]) == 10);
assert(add_all([5, 10]) == 45);
assert(add_all([9, 10]) == 19);
assert(add_all([0, 0]) == 0);
assert(add_all([-1, 1]) == 0);


function is_it_true(args) {
    return (typeof args == 'boolean');
}
assert(is_it_true(true));
assert(is_it_true(false));
assert(!is_it_true('true'));
assert(!is_it_true(1));
assert(!is_it_true('false'));


function largest_of_four(arr) {
    return arr.map(arr => arr.reduce((a, n) => (n > a ? n : a), arr[0]));
}
assert(JSON.stringify(largest_of_four([[13, 27, 18, 26], [4, 5, 1, 3], [32, 35, 37, 39], [1000, 1001, 857, 1]]) == '[27,5,39,1001]'));


function isAnagram(test, original) {
    if (test == "" && original == "") return true;
    if (test.length != original.length) return false;

    test = test.toLowerCase();
    original = original.toLowerCase();
    let idx = test.indexOf(original[0]);
    if (idx == -1) return false;
    return isAnagram(test.slice(0, idx) + test.slice(idx + 1), original.slice(1));
};
assert(isAnagram("foefet", "toffee"));
assert(isAnagram("Buckethead", "DeathCubeK"));
assert(isAnagram("Twoo", "WooT"));
assert(!isAnagram("dumble", "bumble"));
assert(!isAnagram("ound", "round"));
assert(!isAnagram("apple", "pale"));

